﻿namespace Yes.Contracts
{
    public class BoundedContexts
    {
        public const string CREDIT_APPLICATION_API = "credit-application-api";
        public const string SMS_API = "sms-api";
        public const string CREDIT_ORGANIZATION_FAKE = "credit-organization-fake";
        public const string MONEY_MAN_API = "money-man-api";
        public const string VSEGDA_DA_API = "vsegda-da-api";
        public const string URALSIB_API = "uralsib-api";
        public const string TINKOFF_API = "tinkoff-api";
        public const string ZAYMIGO_API = "zaymigo-api";
        public const string SOVCOMBANK_API = "sovcombank-api";
        public const string VOSTBANK_API = "vostbank-api";
        
    }
}